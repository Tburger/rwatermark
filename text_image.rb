module TextImage
  require 'rmagick' # image processing gem, based on ImageMagick

  ##
  # Create an image in memory of grey text with black and white shadow.
  # See http://www.imagemagick.org/Usage/fonts/#mask
  def self.with_shadow(x_offset, y_offset, font_size, text)
    # Create a new image in memory with transparent canvas. This image
    # is used to measure the text string. The unit is pixel.
    mark = Magick::Image.new(1,1) {self.background_color = "none"}

    # The draw object is used to to execute the image manipulation methods
    # to add elements to an image like text.
    draw = Magick::Draw.new
    draw.annotate(mark, 0, 0, 0, 0, text) do
      draw.gravity = Magick::NorthWestGravity
      draw.pointsize = font_size
      draw.font_family = "Times"
      draw.font_weight = Magick::NormalWeight
      draw.stroke = "none" # remove stroke
    end

    # https://rmagick.github.io/draw.html#get_type_metrics
    type_metrics = draw.get_type_metrics(mark, text)
    text_pos = []
    text_pos << [x_offset, y_offset]
    text_pos << [2 * x_offset + type_metrics.width.to_i, 2 * y_offset + type_metrics.height.to_i]
    mark.resize!(3 * x_offset + 2 * type_metrics.width.to_i, 3 * y_offset + 2 * type_metrics.height.to_i)

    # First writing the background in black, then with a little offset
    # write the text in white. Finally, the gray lettering is drawn in
    # the middle. Second offset lettering with calculated coordinates.
    text_pos.each do |pos|
      draw.fill = "black"
      draw.annotate(mark, 0, 0, pos[0]+2, pos[1]+2, text)
      draw.fill = "white"
      draw.annotate(mark, 0, 0, pos[0]+0, pos[1]+0, text)
      draw.fill = "grey"
      draw.annotate(mark, 0, 0, pos[0]+1, pos[1]+1, text)
    end
    mark  # Don't forget to destroy the image!
  end

  # Text without filling, just the outline of the font. Ajust the color.
  # See http://www.imagemagick.org/Usage/fonts/#mask
  def self.thin_stroke(x_offset, y_offset, font_size, text)
    # Create a new image in memory with transparent canvas. This image
    # is used to measure the text string. The unit is pixel.
    mark = Magick::Image.new(1,1) {self.background_color = "none"}

    # The draw object is used to to execute the image manipulation methods
    # to add elements to an image like text.
    draw = Magick::Draw.new
    draw.annotate(mark, 0, 0, 0, 0, text) do
      draw.gravity = Magick::NorthWestGravity
      draw.pointsize = font_size
      draw.font_family = "Times"
      draw.font_weight = Magick::NormalWeight
      # https://rmagick.github.io/imusage.html#color_names
      # https://en.wikipedia.org/wiki/X11_color_names
      draw.stroke = "Silver"  #"LightGray"  #azure"
      draw.stroke_width = 1
      draw.fill = "none"
    end

    # https://rmagick.github.io/draw.html#get_type_metrics
    type_metrics = draw.get_type_metrics(mark, text)
    text_pos = []
    text_pos << [x_offset, y_offset]
    text_pos << [2 * x_offset + type_metrics.width.to_i, 2 * y_offset + type_metrics.height.to_i]
    mark.resize!(3 * x_offset + 2 * type_metrics.width.to_i, 3 * y_offset + 2 * type_metrics.height.to_i)

    text_pos.each do |pos|
      draw.annotate(mark, 0, 0, pos[0]+0, pos[1]+0, text)
    end
    mark  # Don't forget to destroy the image!
  end

end
