module Composite
  require 'rmagick' # image processing gem, based on ImageMagick

  def self.tiled(watermark, file, overwrite, strip_meta_data, resize)
    # Read the image in the memory with RMagick
    img = Magick::Image.read(file).first
    img.strip! if strip_meta_data

    # There are different operators with very different properties to manipulate an image.
    # You could try one of these: AtopCompositeOp, DissolveCompositeOp, OverCompositeOp,
    # OverlayCompositeOp or SoftLightCompositeOp
    # From my point of view the SoftLightCompositeOp creates the best result.
    img.composite_tiled!(watermark, composite_op = Magick::SoftLightCompositeOp)

    # Use the change_geometry method to resize an image with constraints such
    # as "maintain the current proportions."
    if 0 < resize
      img.change_geometry(resize) {|cols, rows, img| img.resize!(cols, rows)}
    end

    if overwrite
      img.write(file)
    else
      img.write(File.join(File.dirname(file), 'marked_' + File.basename(file)))
    end
    img.destroy!
  rescue StandardError => e
    puts Rainbow(e.message.to_s).red.bright
  end

end