## Brief

A simple ruby command line application to add a watermark image to one or more image files. The script provide a resize
option of the destination image and can remove meta data.

## Usage
This script add a watermark to one or more given images, whereas the script do not overwrite a image per default. Each  
file will be prefixed with 'marked_' The watermark file must be provided from the user. There are plenty of options how  
a watermark should look like. So the best way would be to use a professional image software to create a watermark image  
file. But keep in mind, it should be a image with an alpha channel. The module "text_image.rb" shows how to create such  
a watermark file programmatically. Feel free to use it.  

The script provide a resize option of the destination image. Per default no resizing take place. The value is the
longest edge of the image. The resize operation do not change the aspect ration of the image. The '-s' or '--strip'
option remove the metadata of the image.

### Examples
The following command add the watermark file 'watermark.png' to the images img1 to img3:  
**ruby watermark.rb -m watermark.png img1.jpg img2.jpg img3.jpg**  

Add the watermark file to the 'img.jpg' and resize the image to 1024 pixel and strip all metadata.  
**ruby watermark.rb -m watermark.png img.jpg -s -r1024**  

As described above, but the target file will be overwritten. Switch -o | --overwrite  
**ruby watermark.rb -m watermark.png img.jpg -s -r1024 -o**  

### Available Program Options:
    -m, --watermark [file]           Watermark image file
    -s, --strip                      If set, all meta data will be removed
    -r, --resize [number]            If given, resize the destination image
    -o, --overwrite                  If set, the image will be overwritten
    -h, --help                       Show this message

### Install
1) A Ruby script interpreter must be available. Check
with **ruby --version** I recommend to create and use gemset, e.g.: watermark.
2) Execute **bundle install** to install dependencies
3) Check **ruby watermark.rb --help**