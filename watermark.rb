#!/usr/bin/env ruby
#
# This script add a watermark to one or more given images. The watermark file
# must be provided from the user. There are plenty of options how a watermark
# should look like. So the best way would be to use a professional image software
# to create a watermark image file. But keep in mind, it should be a image with
# an alpha channel. The module "text_image.rb" shows how to create such a file
# programmatically. Feel free to use it.
# The script provide a resize option of the destination image. Per default
# no resizing take place. The value is the longest edge of the image. The
# resize operation do not change the aspect ration of the image.
# The '-s' or '--strip' option remove the metadata of the image.
#
# # https://www.rubydoc.info/gems/rmagick/Magick%2FImage:watermark
# # https://github.com/minimagick/minimagick
#
# TODO: configurable workflow
# TODO: https://rmagick.github.io/stegano.rb.html
# TODO: Replace rmagick with an other High-level image processing wrapper, e.g.: libvips
# https://github.com/janko/image_processing
# https://rubygems.org/gems/ruby-vips
#

require 'rainbow' # gem, that colors the output
require 'rmagick' # image processing gem, based on ImageMagick
require_relative 'options'  # Cmd line options
require_relative 'composite'  # module composite

######################################
# main
if ARGV.size.zero?
  puts Rainbow('Error: A image file is needed! Try --help').red.bright
  exit(-1)
end

watermark = Magick::Image.read($opts[:watermark]).first
ARGV.each do |f|
  file = File.absolute_path(f)
  if File.file?(file)
    Composite.tiled(watermark, file, $opts[:overwrite], $opts[:strip], $opts[:resize])
  else
    puts Rainbow("Error: Image file '#{file}' does not exists!").red.bright
  end
end
# rmagic waste a lot of memory. My solution is to call the destroy method.
watermark.destroy!
