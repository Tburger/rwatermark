require 'optparse'

# Cmd line options as global var. Sorry, I do not know a better solution. :-(
$opts = {}

# Create a OptionParser object, pass cmd line args parse it and fill hash.
parser = OptionParser.new do |opts|
  opts.banner = "\nUsage: #{$PROGRAM_NAME} [options]\n" \
      "  Script adds a given watermark to one or more given images.\n" \
      "  See command line option below.\n" \
      "  Examples: \n" \
      "  ruby #{$PROGRAM_NAME} -m watermark.png img1.jpg img2.jpg img3.jpg\n" \
      "  ruby #{$PROGRAM_NAME} -m watermark.png out.jpg -s -r1024\n"
      "  ruby #{$PROGRAM_NAME} -m watermark.png out.jpg -o\n"
  opts.separator "Available Program Options:"
  opts.on_tail("-h", "--help", "Show this message") do
    puts opts
    exit
  end

  opts.on('-m', '--watermark [file]', String, 'Watermark image file') do |v|
    $opts[:watermark] = v unless v.nil?
  end

  $opts[:strip] = false
  opts.on('-s', '--strip', TrueClass, 'If set, all meta data will be removed') do |v|
    $opts[:strip] = v.nil? ? false : v
  end

  $opts[:resize] = 0  # That means do nothing
  opts.on('-r', '--resize [number]', Numeric, 'If given, resize the destination image') do |v|
    $opts[:resize] = v.to_i unless v.nil?
  end

  $opts[:overwrite] = false
  opts.on('-o', '--overwrite', TrueClass, 'If set, the image will be overwritten') do |v|
    $opts[:overwrite] = v.nil? ? false : v
  end
end

parser.parse!

# Now check options. Watermark file is mandatory ...
unless $opts.key?(:watermark)
  puts Rainbow("Error: Watermark file (-m) is missing! Try --help").red.bright
  exit(-1)
end

# ... and must be a regular file that exists.
unless File.file?($opts[:watermark])
  puts Rainbow("Error: Watermark file '#{$opts[:watermark]}' does not exists!").red.bright
  exit(-1)
end