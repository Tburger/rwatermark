require 'minitest/autorun'

class TextImageTest < MiniTest::Test
  require_relative '../text_image'

  TEST_DIR = './tmp'

  def setup
    Dir.mkdir(TEST_DIR) unless Dir.exist?(TEST_DIR)
  end

  def teardown
    # Do nothing
  end

  def test_generate_with_shadow
    # Example to create a text based image.
    fn = File.join(TEST_DIR,'watermark_text_with_shadow.png')
    File.delete(fn) if File.file?(fn)
    watermark = TextImage.with_shadow(10, 10, 46, 'copyright')
    watermark.write(fn)
    assert_equal true, File.file?(fn)
    watermark.destroy!
  end

  def test_generate_thin_stroke
    # Example to create a text based image.
    fn = File.join(TEST_DIR, 'watermark_text_thin_stroke.png')
    File.delete(fn) if File.file?(fn)
    watermark = TextImage.thin_stroke(10, 10, 58, 'copyright')
    watermark.write(fn)
    assert_equal true, File.file?(fn)
    watermark.destroy!
  end

end