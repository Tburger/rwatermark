require 'minitest/autorun'

class CompositeTest < MiniTest::Test
  require 'rainbow'
  require 'rmagick'
  require_relative '../composite'

  TEST_DIR = './tmp'
  WATERMARK_FILE = './img/wm_with_shadow.png'

  def setup
    Dir.mkdir(TEST_DIR) unless Dir.exist?(TEST_DIR)

    # Read watermark file into Magick::Image object.
    @watermark = Magick::Image.read(WATERMARK_FILE).first
  end

  def teardown
    # Do nothing
  end

  def test_tiled_no_overwrite_no_strip
    src_file = './img/test1.jpg'
    dest_file = File.join(TEST_DIR, 'test.jpg')
    overwrite = false
    strip_meta_data = false
    resize = 0

    # Copy file and compare file size. Must be equal.
    FileUtils.cp(src_file, dest_file)
    assert_same(File.size(src_file), File.size(dest_file))

    # Read watermark file into Magick::Image object.
    assert_instance_of(Magick::Image, @watermark)

    # A new file will be written. It is unmistakably that the file
    # size is different as the original file size.
    Composite.tiled(@watermark, dest_file, overwrite, strip_meta_data, resize)
  end

  def test_tiled_overwrite_no_strip
    src_file = './img/test1.jpg'
    dest_file = File.join(TEST_DIR, 'test.jpg')
    overwrite = true
    strip_meta_data = false
    resize = 0

    # Copy file and compare file size. Must be equal.
    FileUtils.cp(src_file, dest_file)
    assert_same(File.size(src_file), File.size(dest_file))

    assert_instance_of(Magick::Image, @watermark)

    # The original file will be overwritten. The file will has a
    # different size as the original file size.
    Composite.tiled(@watermark, dest_file, overwrite, strip_meta_data, resize)
  end

  def test_tiled_no_overwrite_strip
    src_file = './img/test1.jpg'
    dest_file = File.join(TEST_DIR, 'test.jpg')
    overwrite = false
    strip_meta_data = true
    resize = 0

    # Copy file and compare file size. Must be equal.
    FileUtils.cp(src_file, dest_file)
    assert_same(File.size(src_file), File.size(dest_file))

    assert_instance_of(Magick::Image, @watermark)
    Composite.tiled(@watermark, dest_file, overwrite, strip_meta_data, resize)
  end

  def test_tiled_overwrite_strip
    src_file = './img/test1.jpg'
    dest_file = File.join(TEST_DIR, 'test.jpg')
    overwrite = true
    strip_meta_data = true
    resize = 0

    # Copy file and compare file size. Must be equal.
    FileUtils.cp(src_file, dest_file)
    assert_same(File.size(src_file), File.size(dest_file))

    assert_instance_of(Magick::Image, @watermark)
    Composite.tiled(@watermark, dest_file, overwrite, strip_meta_data, resize)
  end

  def test_tiled_resize
    src_file = './img/test1.jpg'
    dest_file = File.join(TEST_DIR, 'test.jpg')
    overwrite = true
    strip_meta_data = true
    resize = 512

    # Copy file and compare file size. Must be equal.
    FileUtils.cp(src_file, dest_file)
    assert_same(File.size(src_file), File.size(dest_file))

    assert_instance_of(Magick::Image, @watermark)
    Composite.tiled(@watermark, dest_file, overwrite, strip_meta_data, resize)

    #TODO: add test to check resize
  end
end